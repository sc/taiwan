# Drivers of cultural diversification in precipitous terrains: can morphological characteristics explain language diversity in the Taiwan mountain belt?
 ---Phase 2: post-AGU Dec19--

## DATA check:
- Check village location with new references (Zeitoun mail): more villages in high mountains?
- Check Language distribution in original publi (Zeitoun mail)

## DATA more:

- paths between settlements matrix: add 1) dnivel du path + total up + total down; 2) "less effort path" (QGIs python code); 3) max. releif crossed (print relief map value on path vector) ; 4) idem for slope
- Existing real paths from JapMaps

## MODEL idea:
- formula combining distance avec a) denivele ou total up ou relief b) bassin versants
- Distance  to the data (find a distance that gives a "%" of fit)
- Limit the number of neighbors (to avoid the 1192x1192 matric)

--------------
## En cours: 
Avec les donnes/model actuel, continuer a simuler pour affiner le ABC (fair avec P=1 =>supprimer P et Tstep plus longs for exmple: [30:250]

## Least-cost path
Testing with Qgis but ArcGIS seems to have a better developped and tested algorithms with nice detailed documentation... considering doing it with ArcGIS
Basics: "the four basics" for deriving the least cost path: 
    a-source raster: source cells(vilages) = 1 --- the rest = nodata;  
    b-cost raster: cost of a cell is the sum of diferent costs. Cost is actual or relative. Can factorise to give more importance to certain factors.
    c-cost distance measures, 
    d-an algorithm (merci); "....many different ways to connect two cells and they do not have to be immediate neighbors..."
Workflow: 1) 
Problems: it take ages to compute... 
Approaches to be choosen: least cost path do not 
Resources (web): 
Overview of Least Cost Path Analysis (with example on paleolitik sites conectivity) https://www.gislounge.com/overview-least-cost-path-analysis/
ArcGIS workflow: https://desktop.arcgis.com/en/analytics/case-studies/cost-lesson-3-desktop-creating-a-least-cost-path.htm

Papers:
Least-Cost Modelling and Landscape Ecology: Concepts, Applications, and Opportunities  https://link.springer.com/article/10.1007/s40823-016-0006-9

## TO DO  Dec 2020
1) Nouvelles matrices
2) Score /performance/Match metric
3) Draft paper

##Score performance idea:
IDEA: adopt inverse approach, as done in geophysics (natural neighbourhood), to asses the best fitting parameters.
Refs (see pdf in folder): 
    1) Original paper setting the mathematial expressions (two papers): Geophysical inversion with a neighbourhood algorithm_ Malcolm Sambridge 1999
    2) Implemented in Pecube code (that's how I got there...; see "inversion" section): Quantifying rates of landscape evolution and tectonic processes by thermochronology and numerical modeling of crustal heat transport using PECUBE _ JeanBraun et al. 2012
