# Drivers of cultural diversification in precipitous terrains 
## Can morphological characteristics explain language diversity in the Taiwan mountain Belt?

A simple model of cultural evoution whe transmission is impacted by geomorphological characteristics.

* `doc/` : general info about the study
* `dev/` : model
* `data/` : raw data
