#' Alpha
#'
#' A simple function to change the opacity of a color
#' @param  color the name or idea of a R color
#' @param  alpha a value in [0,1] defining the opacity wanted.
source("../taiwanpackage.R")
library(hdrcde)


#=====
#code used to generate psoter stuff
alces=rbind(getBackData("data/alces","^alces*"))
abies=getBackData("data/","^abies*")
allsim=rbind(alces,abies)
allsim$meanscore=sqrt(as.numeric(allsim$nogeo)^2+as.numeric(allsim$bassin)^2)
posterior=allsim[order(allsim$meanscore),][1:100,]

pdf("posterior_alpha.pdf")
plot2dens(A=posterior$alpha,xlab="alpha",prior=allsim$alpha,main="Posterior distribution of alpha",cols=c(alpha("white",1),alpha("green",.2),alpha("white",.5)),hdr=T,xaxis=F)
axis(1)
legend("topright",legend=c("prior","posterior"),fill=c("white",alpha("green",.2)))
abline(v=0,lty=2)
dev.off()

pdf("posterior_mu.pdf")
plot2dens(A=posterior$mu,xlab="mu",prior=allsim$mu,main="Posterior distribution of mu",cols=c(alpha("white",1),alpha("green",.2),alpha("white",.5)),hdr=T,xaxis=F)
axis(1)
legend("topright",legend=c("prior","posterior"),fill=c("white",alpha("green",.2)))
abline(v=0,lty=2)
dev.off()

pdf("posterior_tstep.pdf")
plot2dens(A=posterior$t_step,xlab="mu",prior=allsim$t_step,main="Posterior distribution of mu",cols=c(alpha("white",1),alpha("green",.2),alpha("white",.5)),hdr=T,xaxis=F)
axis(1)
legend("topright",legend=c("prior","posterior"),fill=c("white",alpha("green",.2)))
abline(v=0,lty=2)
dev.off()

best=allsim[which.min(allsim$meanscore),]
res= wrapper_evolveVillages(tstep=best$t_step,allv=allv,alldist=alldist,mu=best$mu,P=best$P,alpha=best$alpha)

colorsFun<-function(datacol)colorRampPalette(c("blue","yellow","red"))(1000)[as.numeric(cut(datacol,breaks = 1000))]
plot(allsim$nogeo,allsim$bassin,col=colorsFun(allsim$t_step))


alpha2=getBackData("alpha2","^alpha2alces*")
alpha2$meanscore=sqrt(as.numeric(alpha2$nogeo)^2+as.numeric(alpha2$bassin)^2)

neutral=getBackData(".","^abiesNeutral*")
neutral$meanscore=sqrt(as.numeric(neutral$nogeo)^2+as.numeric(neutral$bassin)^2)

neutral=getBackData(".","^abiesNeutral*")
neutral$meanscore=sqrt(as.numeric(neutral$nogeo)^2+as.numeric(neutral$bassin)^2)

neutral2=getBackData("neutralalces","^neutralalces*")
neutral2$meanscore=sqrt(as.numeric(neutral2$nogeo)^2+as.numeric(neutral2$bassin)^2)

neutral=getBackData("neutral","^alpha2alces*")
neutral$meanscore=sqrt(as.numeric(neutral$nogeo)^2+as.numeric(neutral$bassin)^2)

neutralsc=getBackData("neutralShortcutted/","^neutralShortcuttedalces*")
neutralsc$meanscore=sqrt(as.numeric(neutralsc$nogeo)^2+as.numeric(neutralsc$bassin)^2)

pdf("compare_onesimu.pdf",width=8,height=6)
par(mfrow=c(1,2))
plotFamillies(res$simu,pch=20,main="simulation")
plotFamillies(res$orig,pch=20,main="data")
dev.off()

par(mfrow=c(1,2))
plotFamillies(resE$simu,pch=20,main="simulation")
plotFamillies(resE$orig,pch=20,main="data")

resE= wrapper_evolveVillages(tstep=best$tstep,allv=allv,alldist=alldist,mu=best$mu,P=.6,alpha=best$alpha)

par(mfrow=c(1,2))
plot3Dfamillies(resE$simu,pch=20,main="simulation",size=6)
plot3Dfamillies(resE$orig,pch=20,main="data",size=6)

#====
