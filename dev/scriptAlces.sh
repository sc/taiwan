#!/bin/bash
scriptfile=$1
nsimu=$2
name=$3
ncore=7

for node in alces{01..12}; do 
    echo "sending job to $node"
    echo $node "cd projects/tawain/dev/ ; Rscript $scriptfile $ncore $nsimu ${name}${node} > ${name}log${node} &"; 
    ssh $node "cd ~/projects/tawain/dev/ ; Rscript $scriptfile $ncore $nsimu ${name}${node} > ${name}log${node} &"; 
    echo "done"
done
