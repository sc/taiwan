#!/usr/bin/python

#A simple evolutionary model to study the evolution of workshop amphora production
#

import random 
import math
import csv
from model.Settlement import Settlement #import the agent class
import pandas as pd
import numpy as np 

#Definition of a simulation
class Simulation(object):
    #"sd",12.3795766686627,8.5207422211249,9.83854926282588,11.6498468434151,13.2438062309636

    n_st=-1 ##if no number of workshop given we us 5
    max_time= 10000
    outfile= "output"
    model= -1


    mu=.001
    tstep=1000
    initialfam=1


    #alldist$relDistance=(alldist$Distance - min(alldist$Distance))/(max(alldist$Distance) - min(alldist$Distance))

    #allRelDist=lapply(villages$Name,function(i) getInvertRelativeProba(alldist$Distance[alldist$InputID == i]))
    #names(allRelDist)=villages$Name

    world=list()
    world_list=dict()
    world_lim=list()

    def __init__(self,tstep,mu,initialfam=initialfam,filevillage="../data/Villages_all_v4.csv",getfull=False):
        self.mu=mu
        self.tstep=tstep
        self.listvillages=pd.read_csv(filevillage)
        self.listvillages= self.listvillages[["Name","Elevation","xcoord","ycoord","Lang_famil","Drainage basin"]] #simulated dataset (we keep only 
        self.nsettlement=len(self.listvillages.index)
        self.listvillages["Lang_famil"]=[initialfam]*self.nsettlement
        self.alldist=pd.read_csv("../data/Village distance matrix.csv")

    def computeRelativeDist(self):
        print("start computing all relative distance")
        self.allRelDist=dict.fromkeys(self.listvillages["Name"])
        for k in self.allRelDist : self.allRelDist[k]=self.getInvertRelativeProba(self.alldist["Distance"][self.alldist["InputID"]==k])
        print("done")

    def getDiversity(self,listtraits,threshold):
        traits=np.unique(listtraits)
        freqtraits=np.unique(listtraits,return_counts=True)
        return(len(freqtraits[0][freqtraits[1]>threshold]))
    #given a absolute distance, return a relative distance
    def getInvertRelativeProba(self,i):
        u=sum(i)/i
        probas=u/sum(u)
        return(pow(probas,2)/sum(pow(probas,2)))

    def run(self,finallist=False): ##main function of the class Experiment => run a simulation
        corpus=len(set(self.listvillages["Lang_famil"])) #initial culture idea, we will store the total number of different culture to get new idea when generating new ones
        div=[corpus]
        for i in range(self.tstep):
            print(i)
            inov=np.random.uniform(0,1,self.nsettlement) #random inovation
            newc=sum(inov < self.mu) #get the number of new cultures
            copyc=sum(inov >= self.mu) #get the number of social learner 
            self.listvillages["Lang_famil"][inov>=self.mu]=np.random.choice(self.listvillages["Lang_famil"],copyc) 
            self.listvillages["Lang_famil"][inov<self.mu]=list(range(corpus+1,corpus+1+newc))
            corpus=corpus+newc
            div=div+[self.getDiversity((self.listvillages["Lang_famil"]),0)]
        if finallist:return({"listvillages":self.listvillages,"div":div})
        return(self.listvillages)

    def runDist(self,finallist=False): ##main function of the class Experiment => run a simulation
        self.computeRelativeDist()
        corpus=len(set(self.listvillages["Lang_famil"])) #initial culture idea, we will store the total number of different culture to get new idea when generating new ones
        div=[corpus]
        for i in range(self.tstep):
            print(i)
            inov=np.random.uniform(0,1,self.nsettlement) #random inovation
            newc=sum(inov < self.mu) #get the number of new cultures
            copyc=sum(inov >= self.mu) #get the number of social learner 
            tocopy=self.listvillages["Name"][inov>=self.mu]
            newfamilies=[]
            for k in tocopy:
                j=np.random.choice(self.alldist["TargetID"][self.alldist["InputID"]==k],1,p=self.allRelDist[k])
                newfami=self.listvillages["Lang_famil"][self.listvillages["Name"] == j[0]]
                newfamilies=newfamilies+[int(newfami)]
            self.listvillages["Lang_famil"][inov>=self.mu]=newfamilies
            self.listvillages["Lang_famil"][inov<self.mu]=list(range(corpus+1,corpus+1+newc))
            corpus=corpus+newc
            div=div+[self.getDiversity((self.listvillages["Lang_famil"]),0)]
        if finallist:return({"listvillages":self.listvillages,"div":div})
        return(self.listvillages)


