import sys, getopt
from model.theisland import Simulation 

if __name__ == "__main__":
    mu=.001 ##mutation probability 1 other 1000 .1 percent
    tstep=20
    print("start")
    main_exp=Simulation(mu=mu,tstep=tstep)

    res=main_exp.runDist(finallist=True)
    res['listvillages'].to_csv("res.csv")
    print(res['div'])

